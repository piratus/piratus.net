const BROWSERS = process.env.BROWSERSLIST || "> 1%, last 2 versions, Firefox ESR, Opera 12.1";

const CSSNEXT_CONFIG = {
  browsers: BROWSERS,
  warnForDuplicates: false
};

const CSSNANO_CONFIG = {
  preset: 'default',
  browsers: BROWSERS
};

module.exports = {
  plugins: [
    require('postcss-cssnext')(CSSNEXT_CONFIG),
    require('cssnano')(CSSNANO_CONFIG)
  ],
  map: false
};
